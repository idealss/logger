unit ideal.logger.viewer.forms.dt.main;

interface

uses
  System.SysUtils,
  System.Types,
  System.UITypes,
  System.Classes,
  System.Variants,
  System.Generics.Collections,
  System.DateUtils,
  ideal.logger.viewer.stores.files,
  ideal.logger.viewer.stores.open,
  ideal.logger.viewer.stores.views,
  ideal.logger.viewer.types,
  ideal.logger.viewer.tree,
  ideal.logger.viewer.actions,
  FMX.Types,
  FMX.Controls,
  FMX.Forms,
  FMX.Graphics,
  FMX.Dialogs,
  FMX.Objects,
  FMX.Layouts,
  FMX.TreeView,
  FMX.Ani,
  FMX.StdCtrls,
  FMX.Effects,
  FMX.TabControl,
  FMX.Memo.Types,
  FMX.Controls.Presentation,
  FMX.ScrollBox,
  FMX.Memo,
  FMX.TMSBaseControl,
  FMX.TMSTreeViewBase,
  FMX.TMSTreeViewData,
  FMX.TMSCustomTreeView,
  FMX.TMSTreeView,
  FMX.Menus, 
  FMX.Edit;

type
  TFileTreeViewItem = class(TTreeViewItem)
  private
    FFileSize : TText;
    FItem: TFileRecord;
    procedure SetItem(const Value: TFileRecord);
  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;
    property Item : TFileRecord read FItem write SetItem;
  end;

  TfrmMain = class(TForm)
    rectHeader: TRectangle;
    txtMenu: TText;
    tvFiles: TTreeView;
    tviApplications: TTreeViewItem;
    tviDownloads: TTreeViewItem;
    tviOther: TTreeViewItem;
    txtOpen: TText;
    dlgOpen: TOpenDialog;
    splLeft: TSplitter;
    seMenu: TShadowEffect;
    seOpen: TShadowEffect;
    txtCaption: TText;
    splRight: TSplitter;
    txtWarnings: TText;
    txtErrors: TText;
    txtTraceDebug: TText;
    txtInfo: TText;
    seErrors: TShadowEffect;
    seTraceDebug: TShadowEffect;
    seInfo: TShadowEffect;
    seWarnings: TShadowEffect;
    loContent: TLayout;
    rrErrors: TRoundRect;
    rrWarnings: TRoundRect;
    rrTraceDebug: TRoundRect;
    rrInfo: TRoundRect;
    tvLogData: TTMSFMXTreeView;
    loDetails: TLayout;
    DetailSplitter: TSplitter;
    txtDetail: TMemo;
    tvDataViews: TTMSFMXTreeView;
    StyleBook1: TStyleBook;
    Splitter1: TSplitter;
    StatusBar1: TStatusBar;
    txtClearFilters: TText;
    pmLogData: TPopupMenu;
    pmDataViews: TPopupMenu;
    pmDetails: TPopupMenu;
    loToolbar: TLayout;
    txtSearch: TEdit;
    txtSearchPrior: TText;
    txtSearchNext: TText;
    txtFilterInclusive: TText;
    txtFilterExclusive: TText;
    miDeleteView: TMenuItem;
    miReverseView: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure txtMenuClick(Sender: TObject);
    procedure txtOpenClick(Sender: TObject);
    procedure tvFilesChange(Sender: TObject);
    procedure txtCaptionClick(Sender: TObject);
    procedure txtTraceDebugClick(Sender: TObject);
    procedure txtInfoClick(Sender: TObject);
    procedure txtErrorsClick(Sender: TObject);
    procedure txtWarningsClick(Sender: TObject);
    procedure tvLogDataBeforeDrawNode(Sender: TObject; ACanvas: TCanvas;
      ARect: TRectF; ANode: TTMSFMXTreeViewVirtualNode; var AAllow,
      ADefaultDraw: Boolean);
    procedure tvLogDataAfterSelectNode(Sender: TObject;
      ANode: TTMSFMXTreeViewVirtualNode);
    procedure tvDataViewsAfterSelectNode(Sender: TObject;
      ANode: TTMSFMXTreeViewVirtualNode);
    procedure txtClearFiltersClick(Sender: TObject);
    procedure tvLogDataKeyDown(Sender: TObject; var Key: Word;
      var KeyChar: Char; Shift: TShiftState);
    procedure txtSearchChange(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure txtSearchNextClick(Sender: TObject);
    procedure txtSearchPriorClick(Sender: TObject);
    procedure txtFilterInclusiveClick(Sender: TObject);
    procedure txtFilterExclusiveClick(Sender: TObject);
    procedure txtSearchKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure miDeleteViewClick(Sender: TObject);
    procedure miReverseViewClick(Sender: TObject);
    procedure pmDataViewsPopup(Sender: TObject);
  strict private
    FNodeViewMap : TDictionary<TTMSFMXTreeViewNode, IFilterView>;
    FFilesWidth : Single;
    FSelectedFile : TFileRecord;
    FCurrentSearchLine : integer;
    FLastSearchWasNext : boolean;
    FSelectedNode: TTMSFMXTreeViewVirtualNode;
    
    procedure StartSearch;
    procedure SearchNext;
    procedure SearchPrior;

    function FindNodeForID(ID : UInt64) : TTMSFMXTreeViewNode;
    function FindTextInNode(Node : TTMSFMXTreeViewNode) : TTMSFMXTreeViewNode;

    procedure CopyMessageToClipboard(Sender : TObject);
    procedure CopyDataToClipboard(Sender : TObject);
    procedure ResetLogDataPopupMenu;
    procedure UpdateLogDataPopupMenu;
    procedure RebuildTree;
    procedure RebuildViews;
    procedure LoadTree;
    procedure FillFromParent(tviParent : TTreeViewItem; Files : TList<TFileRecord>);
    procedure UpdateFiles(Files : TList<TFileRecord>);
    procedure UpdateAppFiles(Files : TDictionary<string, TList<TFileRecord>>);
    procedure UpdateDownloads(Files : TList<TFileRecord>);
    procedure SetSelectedFile(const Value: TFilerecord);
    function PathToItem(ThreadID : UInt64; Path : TArray<Cardinal>) : ILineGroupInfo;
    property SelectedFile : TFilerecord read FSelectedFile write SetSelectedFile;
    procedure FilterAfterDateTimeClick(Sender : TObject);
    procedure FilterBeforeDateTimeClick(Sender : TObject);
    procedure FilterLineFromClick(Sender : TObject);
    procedure FilterLineToClick(Sender : TObject);
    procedure FilterDeltaMinClick(Sender : TObject);
    procedure FilterDurationMinClick(Sender : TObject);
    procedure FilterDurationMaxClick(Sender : TObject);
    procedure FilterTextIncludesClick(Sender : TObject);
    procedure FilterTextExcludesClick(Sender : TObject);
    procedure FilterThreadExcludeClick(Sender : TObject);
    procedure FilterThreadIncludeClick(Sender : TObject);
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

  end;

const
  LogEntryColors : array[TLogEntryType] of TAlphaColor = ($99BCC7E8, $99E7E8EA, $99F8F7E8, $FFCC9C9C, $99F29F7F, $99C9E1C9, $99FB0000, $99FFFFFF);

var
  frmMain: TfrmMain;

implementation

uses
  System.Character,
  System.Rtti,
  FMX.Platform,
  chimera.json.helpers,
  cocinasync.flux.fmx.actions.ui;

{$R *.fmx}

const
  COL_MESSAGE  = 0;
  COL_LEVEL    = 1;
  COL_DATE     = 2;
  COL_TIME     = 3;
  COL_DURATION = 4;
  COL_DELTA    = 5;
  COL_THREAD   = 6;
  COL_OFFSET   = 7;
  COL_ID       = 8;

procedure TfrmMain.CopyDataToClipboard(Sender: TObject);
var
  clip : IFMXClipboardService;
begin
  if not Assigned(FSelectedNode) then
    exit;
  TPlatformServices.Current.SupportsPlatformService(IFMXClipboardService, clip);
  clip.SetClipboard(TValue.From<String>(FSelectedNode.Text[COL_MESSAGE]));
end;

procedure TfrmMain.CopyMessageToClipboard(Sender: TObject);
var
  clip : IFMXClipboardService;
begin
  if not Assigned(FSelectedNode) then
    exit;
  TPlatformServices.Current.SupportsPlatformService(IFMXClipboardService, clip);

  var id := StrToInt64Def(FSelectedNode.Text[COL_ID],0);
  if id > 0 then
  begin
    var s : string;
    TOpenStore.Data.DetailForLine(id, s);
    clip.SetClipboard(TValue.From<String>(s));
  end;
end;

constructor TfrmMain.Create(AOwner: TComponent);
begin
  inherited;
  FLastSearchWasNext := True;
  FNodeViewMap := TDictionary<TTMSFMXTreeViewNode, IFilterView>.Create;
  FCurrentSearchLine := 0;
end;

destructor TfrmMain.Destroy;
begin
  FNodeViewMap.Free;
  inherited;
end;

procedure TfrmMain.FillFromParent(tviParent: TTreeViewItem;
  Files: TList<TFileRecord>);
var
  i, j: Integer;
begin
  tviParent.BeginUpdate;
  try
    i := 0;
    j := 0;
    while j < Files.Count-1 do
    begin
      if i < tviParent.Count-1 then
      begin
        tviParent.items[i].Text := Files[j].FileName;
      end else
      begin
        var tvi := TFileTreeViewItem.Create(tvFiles);
        tvi.Parent := tviParent;
        tvi.Item := Files[j];
      end;
      inc(i);
      inc(j);
    end;
    while i < tviParent.Count do
    begin
      var tvi := tviParent.Items[i];
      tvi.Parent := nil;
      tvi.Free;
    end;
  finally
    tviParent.EndUpdate;
  end;
end;

procedure TfrmMain.FilterAfterDateTimeClick(Sender: TObject);
begin
  TSetStampAfterFilterAction.Post(TMenuItem(Sender).TagFloat);
end;

procedure TfrmMain.FilterBeforeDateTimeClick(Sender: TObject);
begin
  TSetStampBeforeFilterAction.Post(TMenuItem(Sender).TagFloat);
end;

procedure TfrmMain.FilterDeltaMinClick(Sender: TObject);
begin
  TSetDeltaMinFilterAction.Post(TMenuItem(Sender).Tag);
end;

procedure TfrmMain.FilterDurationMaxClick(Sender: TObject);
begin
  TSetDurationMaxFilterAction.Post(TMenuItem(Sender).Tag);
end;

procedure TfrmMain.FilterDurationMinClick(Sender: TObject);
begin
  TSetDurationMinFilterAction.Post(TMenuItem(Sender).Tag);
end;

procedure TfrmMain.FilterLineFromClick(Sender: TObject);
begin
  TSetLineFromFilterAction.Post(TMenuItem(Sender).Tag);
end;

procedure TfrmMain.FilterLineToClick(Sender: TObject);
begin
  TSetLineToFilterAction.Post(TMenuItem(Sender).Tag);
end;

procedure TfrmMain.FilterTextExcludesClick(Sender: TObject);
begin
  TAddTextExcludeFilterAction.Post(TMenuItem(Sender).Text);
end;

procedure TfrmMain.FilterTextIncludesClick(Sender: TObject);
begin
  TAddTextIncludeFilterAction.Post(TMenuItem(Sender).Text);
end;

procedure TfrmMain.FilterThreadExcludeClick(Sender: TObject);
begin
  TExcludeThreadFilterAction.Post(TMenuItem(Sender).Tag);
end;

procedure TfrmMain.FilterThreadIncludeClick(Sender: TObject);
begin
  TIncludeThreadFilterAction.Post(TMenuItem(Sender).Tag);
end;

function TfrmMain.FindNodeForID(ID: UInt64): TTMSFMXTreeViewNode;
  function FindNodeForIDInList(ID: UInt64; Nodes : TTMSFMXTreeViewNodes): TTMSFMXTreeViewNode;
  begin  
    Result := nil;
    for var i := 0 to Nodes.Count-1 do
    begin
      if StrToInt64Def(Nodes[i].Text[COL_ID],0) >= ID then
        Result := Nodes[i]
      else
        Result := FindNodeForIDInList(ID, Nodes[i].Nodes);
      
      if Assigned(Result) then
        break;
    end;
  end;
begin
  Result := FindNodeForIDInList(ID, tvLogData.Nodes);
end;

function TfrmMain.FindTextInNode(Node: TTMSFMXTreeViewNode): TTMSFMXTreeViewNode;
begin
  Result := nil;
  if Node.Text[COL_MESSAGE].ToUpper.Contains(txtSearch.Text.ToUpper) then
    exit(Node);
  for var i := Node.Nodes.Count-1 downto 0 do
  begin
    Result := FindTextInNode(Node.Nodes[i]);
    if Assigned(Result) then
      break;
  end;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
var
  ary : TArray<Cardinal>;
begin
  FFilesWidth := tvFiles.Width;
  tvLogData.ColumnsAppearance.Stretch := True;

  TFilesStore.Data.RegisterForUpdates<TFilesStore>(Self,
    procedure(Store : TFilesStore)
    begin
      tvFiles.BeginUpdate;
      try
        UpdateFiles(Store.Files);
        UpdateAppFiles(Store.AppFiles);
        UpdateDownloads(Store.Downloads);
      finally
        tvFiles.EndUpdate;
      end;
    end
  );

  TOpenStore.Data.RegisterForUpdates<TOpenStore>(Self,
    procedure(Store : TOpenStore)
    begin
      RebuildTree;
    end
  );

  TViewsStore.Data.RegisterForUpdates<TViewsStore>(Self,
    procedure(Store : TViewsStore)
    begin
      RebuildViews;
    end
  );
end;

procedure TfrmMain.FormDestroy(Sender: TObject);
begin
  TFilesStore.Data.UnregisterForUpdates(Self);
  TOpenStore.Data.UnregisterForUpdates(Self);
end;

procedure TfrmMain.FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
  Shift: TShiftState);
begin
  if (Shift = [ssCtrl]) and (Key = vkF) then
  begin
    StartSearch;
  end;

  if (Shift = []) and (Key = vkF3) then
  begin
    SearchNext;
  end;

  if (Shift = [ssShift]) and (Key = vkF3) then
  begin
    SearchPrior;
  end;
end;

procedure TfrmMain.LoadTree;
  procedure AddItem(group : ILineGroupInfo; Parent : TTMSFMXTreeViewNode);
    function DateOf(dt : TDateTime) : string;
    begin
      DateTimeToString(Result, 'yyyy mm dd', dt);
    end;
    function TimeOf(dt : TDateTime) : string;
    begin
      DateTimeToString(Result, 'hh:mm:ss zzz', dt);
    end;
  var
    Node : TTMSFMXTreeViewNode;
  begin
    Node := tvLogData.AddNode(Parent);
    Node.Text[COL_DATE] := DateOf(group.Timestamp);
    Node.Text[COL_TIME] := TimeOf(group.Timestamp);
    Node.Text[COL_DELTA] := group.Delta.ToString;
    Node.Text[COL_THREAD] := group.Thread.ToString;
    Node.Text[COL_OFFSET] := group.Offset;
    Node.Text[COL_ID] := group.LineFrom.ToString;
    Node.Text[COL_MESSAGE] := group.Text.Replace(#13#10,' | ').Replace(#13,' | ').Replace(#10, ' | ');
    if group.Items.Count > 0 then
    begin
      Node.Text[COL_DURATION] := group.Duration.ToString;
      for var item in group.Items do
      begin
        AddItem(item,Node);
      end;
    end else
    begin
      Node.Text[COL_LEVEL] := group.Level;
      Node.Text[COL_DURATION] := '';
    end;
  end;
begin
  tvLogData.BeginUpdate;
  try
    tvLogData.ClearNodes;
    for var l in TOpenStore.Data.List do
    begin
      AddItem(l, nil);
    end;
  finally
    tvLogData.EndUpdate;
  end;
  if TOpenStore.Data.IsFiltered then
    tvLogData.ExpandAll;
end;

procedure TfrmMain.miDeleteViewClick(Sender: TObject);
begin
  TRemoveFilterViewAction.Post(TViewsStore.Data.Current);
end;

procedure TfrmMain.miReverseViewClick(Sender: TObject);
begin
  TReverseFilterViewAction.Post(TViewsStore.Data.Current);
end;

function TfrmMain.PathToItem(ThreadID : UInt64; Path: TArray<Cardinal>): ILineGroupInfo;
var
  lst : TList<ILineGroupInfo>;
  idx : Cardinal;
  stack : TStack<Cardinal>;
begin
  Result := nil;

  idx := 0;
  lst := TOpenStore.Data.List;

  stack := TStack<Cardinal>.Create;
  try
    repeat
      if idx >= lst.Count then
      begin
        break;
      end;
      Result:= lst.Items[Path[idx]];
      lst := Result.Items;
      inc(idx);
    until idx >= Length(Path);
  finally
    stack.Free;
  end;

end;

procedure TfrmMain.pmDataViewsPopup(Sender: TObject);
begin
  miDeleteView.Enabled := Assigned(TViewsStore.Data.Current) and (TViewsStore.Data.Current <> TViewsStore.Data.Root);
  miReverseView.Enabled := Assigned(TViewsStore.Data.Current) and (TViewsStore.Data.Current <> TViewsStore.Data.Root);
end;

procedure TfrmMain.UpdateLogDataPopupMenu;
  function TimeStampFor(node : TTMSFMXTreeViewNode) : TDateTime;
  begin
    var d := node.Text[COL_DATE].Split([' ']);
    if Length(d) <> 3 then
      raise Exception.Create('Invalid Date in Log');
    Result := EncodeDate(d[0].ToInteger, d[1].ToInteger, d[2].ToInteger);

    var twms := node.Text[COL_TIME].Split([' ']);
    if Length(twms) <> 2 then
      raise Exception.Create('Invalid Time in Log');

    var t := twms[0].Split([':']);

    Result := Result + EncodeTime(t[0].ToInteger, t[1].ToInteger, t[2].ToInteger, twms[1].ToInteger);    
  end;
  
  function DateFormat(DT : TDateTime) : string;
  begin
    DateTimeToString(Result, 'yyyy-mm-nn hh:nn:ss.zzz', DT);
  end;
begin
  ResetLogDataPopupMenu;


  var bAdded := False;
  for var i := 0 to tvLogData.SelectedNodeCount-1 do
  begin
    var node := tvLogData.SelectedNodes[i];
    if StrToInt64Def(node.Text[COL_THREAD],0) > 0 then
    begin
      var mi := TMenuItem.Create(pmLogData);
      mi.Parent := pmLogData;

      mi.Text := 'Include Thread: '+ StrToInt64Def(node.Text[COL_THREAD],0).ToString;
      mi.Tag := StrToInt64Def(node.Text[COL_THREAD],0);
      mi.OnClick := FilterThreadIncludeClick;
      mi.Visible := True;

      mi := TMenuItem.Create(pmLogData);
      mi.Parent := pmLogData;

      mi.Text := 'Exclude Thread: '+ StrToInt64Def(node.Text[COL_THREAD],0).ToString;
      mi.Tag := StrToInt64Def(node.Text[COL_THREAD],0);
      mi.OnClick := FilterThreadExcludeClick;
      mi.Visible := True;
      bAdded := True;
    end;
  end;

  if bAdded then
  begin
    var mi := TMenuItem.Create(pmLogData);
    mi.Parent := pmLogData;
    mi.Text := '-';
    mi.Visible := True;
  end;

  bAdded := False;
  for var i := 0 to tvLogData.SelectedNodeCount-1 do
  begin    
    var node := tvLogData.SelectedNodes[i];
    if TimeStampFor(node) > DEFAULT_STAMP_AFTER then
    begin
      var mi := TMenuItem.Create(pmLogData);
      mi.Parent := pmLogData;

      mi.Text := 'After '+ DateFormat(TimeStampFor(node));
      mi.TagFloat := TimeStampFor(node);
      mi.OnClick := FilterAfterDateTimeClick;
      mi.Visible := True;

      mi := TMenuItem.Create(pmLogData);
      mi.Parent := pmLogData;

      mi.Text := 'Before '+ DateFormat(TimeStampFor(node));
      mi.TagFloat := TimeStampFor(node);
      mi.OnClick := FilterBeforeDateTimeClick;
      mi.Visible := True;
      bAdded := True;
    end;    
  end;

  if bAdded then
  begin
    var mi := TMenuItem.Create(pmLogData);
    mi.Parent := pmLogData;
    mi.Text := '-';
    mi.Visible := True;
  end;

  bAdded := False;    
  for var i := 0 to tvLogData.SelectedNodeCount-1 do
  begin
    var node := tvLogData.SelectedNodes[i];
    if Node.Text[COL_MESSAGE].Trim <> '' then
    begin
      var mi := TMenuItem.Create(pmLogData);
      mi.Parent := pmLogData;
    
      mi.Text := 'Includes: '+ Node.Text[COL_MESSAGE].Trim;
      mi.OnClick := FilterTextIncludesClick;
      mi.Visible := True;

      mi := TMenuItem.Create(pmLogData);
      mi.Parent := pmLogData;
    
      mi.Text := 'Excludes: '+ Node.Text[COL_MESSAGE].Trim;
      mi.OnClick := FilterTextExcludesClick;
      mi.Visible := True;
      bAdded := True;
    end;
  end;    

  if bAdded then
  begin
    var mi := TMenuItem.Create(pmLogData);
    mi.Parent := pmLogData;
    mi.Text := '-';
    mi.Visible := True;
  end;

  bAdded := False;    
  for var i := 0 to tvLogData.SelectedNodeCount-1 do
  begin
    var node := tvLogData.SelectedNodes[i];
    if StrToInt64Def(Node.Text[COL_ID], 0) > DEFAULT_LINE_FROM then
    begin
      var mi := TMenuItem.Create(pmLogData);
      mi.Parent := pmLogData;
    
      mi.Text := 'Line >= '+ StrToInt64Def(Node.Text[COL_ID], 0).ToString;
      mi.Tag := StrToInt64Def(Node.Text[COL_ID], 0);
      mi.OnClick := FilterLineFromClick;
      mi.Visible := True;

      mi := TMenuItem.Create(pmLogData);
      mi.Parent := pmLogData;

      mi.Text := 'Line <= '+ StrToInt64Def(Node.Text[COL_ID], 0).ToString;
      mi.Tag := StrToInt64Def(Node.Text[COL_ID], 0);
      mi.OnClick := FilterLineToClick;
      mi.Visible := True;
      bAdded := True;
    end;
  end;

  if bAdded then
  begin
    var mi := TMenuItem.Create(pmLogData);
    mi.Parent := pmLogData;
    mi.Text := '-';
    mi.Visible := True;
  end;

  bAdded := False;    
  for var i := 0 to tvLogData.SelectedNodeCount-1 do
  begin
    var node := tvLogData.SelectedNodes[i];
    if StrToInt64Def(Node.Text[COL_DELTA], 0) > DEFAULT_DELTA_MIN then
    begin
      var mi := TMenuItem.Create(pmLogData);
      mi.Parent := pmLogData;
    
      mi.Text := 'Delta >= '+ StrToInt64Def(Node.Text[COL_DELTA], 0).ToString+' ms';
      mi.Tag := StrToInt64Def(Node.Text[COL_DELTA], 0);
      mi.OnClick := FilterDeltaMinClick;
      mi.Visible := True;
      bAdded := True;
    end;
  end;
    
  if bAdded then
  begin
    var mi := TMenuItem.Create(pmLogData);
    mi.Parent := pmLogData;
    mi.Text := '-';
    mi.Visible := True;
  end;

  bAdded := False;    
  for var i := 0 to tvLogData.SelectedNodeCount-1 do
  begin
    var node := tvLogData.SelectedNodes[i];
    if StrToInt64Def(Node.Text[COL_DURATION], 0) > DEFAULT_DURATION_MIN then
    begin
      var mi := TMenuItem.Create(pmLogData);
      mi.Parent := pmLogData;
    
      mi.Text := 'Duration >= '+ StrToInt64Def(Node.Text[COL_DURATION], 0).ToString+' ms';
      mi.Tag := StrToInt64Def(Node.Text[COL_DURATION], 0);
      mi.OnClick := FilterDurationMinClick;
      mi.Visible := True;
      
      mi := TMenuItem.Create(pmLogData);
      mi.Parent := pmLogData;
    
      mi.Text := 'Duration <= '+ StrToInt64Def(Node.Text[COL_DURATION], 0).ToString+' ms';
      mi.Tag := StrToInt64Def(Node.Text[COL_DURATION], 0);
      mi.OnClick := FilterDurationMaxClick;
      mi.Visible := True;
      bAdded := True;
    end;
  end;
  
  if bAdded then
  begin
    var mi := TMenuItem.Create(pmLogData);
    mi.Parent := pmLogData;
    mi.Text := '-';
    mi.Visible := True;
  end;
  
  var mi := TMenuItem.Create(pmLogData);
  mi.Parent := pmLogData;
  mi.Text := 'Hide Profile Entries';
  mi.Visible := True;
end;

procedure TfrmMain.RebuildTree;
var
  lst : TList<ILineGroupInfo>;
begin
  TShowBusyAction.Send(Self);
  try
    seErrors.Enabled := TOpenStore.Data.EntryTypeFilter.IsInSet(TLogEntryType.ERROR);
    seInfo.Enabled := TOpenStore.Data.EntryTypeFilter.IsInSet(TLogEntryType.INFO);
    seTraceDebug.Enabled := TOpenStore.Data.EntryTypeFilter.IsInSet(TLogEntryType.DEBUG);
    seWarnings.Enabled := TOpenStore.Data.EntryTypeFilter.IsInSet(TLogEntryType.WARN);

    if TOpenStore.Data.EntryTypeFilter.IsInSet(TLogEntryType.ERROR) then
      rrErrors.Fill.Kind := TBrushKind.Solid
    else
      rrErrors.Fill.Kind := TBrushKind.None;

    if TOpenStore.Data.EntryTypeFilter.IsInSet(TLogEntryType.INFO) then
      rrInfo.Fill.Kind := TBrushKind.Solid
    else
      rrInfo.Fill.Kind := TBrushKind.None;

    if TOpenStore.Data.EntryTypeFilter.IsInSet(TLogEntryType.WARN) then
      rrWarnings.Fill.Kind := TBrushKind.Solid
    else
      rrWarnings.Fill.Kind := TBrushKind.None;

    if TOpenStore.Data.EntryTypeFilter.IsInSet(TLogEntryType.TRACE) then
      rrTraceDebug.Fill.Kind := TBrushKind.Solid
    else
      rrTraceDebug.Fill.Kind := TBrushKind.None;

    LoadTree;
  finally
    THideBusyAction.Send;
  end;
end;

procedure TfrmMain.RebuildViews;
  procedure RenderChildrenAt(Parent : TTMSFMXTreeViewNode; View : IFilterView; var NodeToSelect : TTMSFMXTreeViewNode);
  begin
    for var v in View.Views do
    begin
      var node := tvDataViews.AddNode(Parent);
      node.Text[0] := v.AsText;
      FNodeViewMap.AddOrSetValue(node,v);
      if v = TViewsStore.Data.Current then
        NodeToSelect := node;
      RenderChildrenAt(node, v, NodeToSelect);
    end;
  end;
var
  NodeToSelect : TTMSFMXTreeViewNode;
begin
  tvDataViews.BeginUpdate;
  try
    NodeToSelect := nil;
    tvDataViews.ClearNodes;
    FNodeViewMap.Clear;
    var Root := tvDataViews.AddNode(nil);
    Root.Text[0] := 'All Data';
    FNodeViewMap.AddOrSetValue(Root,TViewsStore.Data.Root);
    RenderChildrenAt(Root, TViewsStore.Data.Root, NodeToSelect);
    if not Assigned(NodeToSelect) then
      NodeToSelect := Root;
  finally
    tvDataViews.EndUpdate;
  end;
  tvDataViews.ExpandAll;
  tvDataViews.SelectNode(NodeToSelect);
end;

procedure TfrmMain.ResetLogDataPopupMenu;
var
  menus : IFMXMenuService;
begin
  pmLogData.Clear;

  TPlatformServices.Current.SupportsPlatformService(IFMXMenuService, menus);

  var mi := TMenuItem.Create(pmLogData);
  mi.Parent := pmLogData;
  mi.Text := 'Copy Message';
  mi.ShortCut := menus.TextToShortCut('Ctrl+C');
  mi.OnClick := CopyMessageToClipboard;
  mi.Visible := True;

  mi := TMenuItem.Create(pmLogData);
  mi.Parent := pmLogData;
  mi.Text := 'Copy Data';
  mi.ShortCut := menus.TextToShortCut('Ctrl+Shift+C');
  mi.OnClick := CopyDataToClipboard;
  mi.Visible := True;

  mi := TMenuItem.Create(pmLogData);
  mi.Parent := pmLogData;
  mi.Text := '-';
  mi.Visible := True;

end;

procedure TfrmMain.SearchNext;
var
  bInSearch : boolean;
  StartingNode, MatchNode, TestNode : TTMSFMXTreeViewNode;
  lstTestedNodes : TList<TTMSFMXTreeViewNode>;
  iSearchLine : Integer;
begin
  FLastSearchWasNext := True;
  bInSearch := False;
  iSearchLine := FCurrentSearchLine;
  lstTestedNodes := TList<TTMSFMXTreeViewNode>.Create;
  try
    // Find the node for the next id to begin searching
    StartingNode := FindNodeForID(FCurrentSearchLine);
    if Assigned(StartingNode) then
    begin
      lstTestedNodes.Add(StartingNode);
      var parent := StartingNode;
      repeat
        var node := parent;
        // look for text in starting node and all siblings at all depths.
        repeat
          if lstTestedNodes.IndexOf(node) < 0 then
          begin
            MatchNode := FindTextInNode(node);
            if Assigned(MatchNode) then
            begin
              tvLogData.SelectNode(MatchNode);
              FCurrentSearchLine := StrToInt64Def(MatchNode.Text[COL_ID],0);
              tvLogData.ScrollToNode(MatchNode);
              exit;
            end;
            lstTestedNodes.Add(node);
          end;
          node := node.GetNextSibling;
        until node = nil;

        // Now walk back to parent and test moving forward
        parent := parent.GetParent;
      until parent = nil;

    end;
  finally
    lstTestedNodes.Free;
  end;
  if FCurrentSearchLine - iSearchLine > 0 then
    SearchNext
  else
    ShowMessage('"'+txtSearch.Text+'" was not found.');  
end;

procedure TfrmMain.SearchPrior;
var
  bInSearch : boolean;
  StartingNode, MatchNode, TestNode : TTMSFMXTreeViewNode;
  lstTestedNodes : TList<TTMSFMXTreeViewNode>;
begin
  FLastSearchWasNext := False;
  bInSearch := False;
  lstTestedNodes := TList<TTMSFMXTreeViewNode>.Create;
  try
    // Find the node for the next id to begin searching
    StartingNode := FindNodeForID(FCurrentSearchLine);
    if Assigned(StartingNode) then
    begin
      lstTestedNodes.Add(StartingNode);
      var parent := StartingNode;
      repeat
        var node := parent;
        // look for text in starting node and all siblings at all depths.
        repeat
          if lstTestedNodes.IndexOf(node) < 0 then
          begin
            MatchNode := FindTextInNode(node);
            if Assigned(MatchNode) then
            begin
              tvLogData.SelectNode(MatchNode);
              FCurrentSearchLine := StrToInt64Def(MatchNode.Text[COL_ID],0);
              tvLogData.ScrollToNode(MatchNode);
              exit;
            end;
            lstTestedNodes.Add(node);
          end;
          node := node.GetPreviousSibling;
        until node = nil;

        // Now walk back to parent and test moving forward
        parent := parent.GetParent;
      until parent = nil;
    end;
  finally
    lstTestedNodes.Free;
  end;
  if FCurrentSearchLine > 0 then
    SearchPrior
  else
    ShowMessage('"'+txtSearch.Text+'" was not found.');  
end;

procedure TfrmMain.SetSelectedFile(const Value: TFilerecord);
begin
  FSelectedFile := Value;
  txtCaption.Text := Value.FileName;
  TShowBusyAction.Send(Self);
  FCurrentSearchLine := 0;
  TLoadViewsForFileAction.Post(Value);
  TViewLogFileAction.Post(Value.Path+Value.Filename);
end;

procedure TfrmMain.StartSearch;
begin
  txtSearch.SetFocus;
  txtSearch.SelectAll;
end;

procedure TfrmMain.tvDataViewsAfterSelectNode(Sender: TObject;
  ANode: TTMSFMXTreeViewVirtualNode);
begin
  if FNodeViewMap.ContainsKey(ANode.Node) then
    TSelectFilterViewAction.Post(FNodeViewMap[ANode.Node]);
end;

procedure TfrmMain.tvFilesChange(Sender: TObject);
begin
  if tvFiles.Selected <> nil then
  begin
    if (tvFiles.Selected is TFileTreeViewItem) then
    begin
      SelectedFile := TFileTreeViewItem(tvFiles.Selected).item;
    end;
  end;
end;

procedure TfrmMain.tvLogDataAfterSelectNode(Sender: TObject;
  ANode: TTMSFMXTreeViewVirtualNode);
var
  ID : UInt64;
  s : string;
begin
  FSelectedNode := ANode;
  id := StrToInt64Def(ANode.Text[COL_ID],0);
  if id > 0 then
    txtDetail.Text := TOpenStore.Data.DetailForLine(id, s);
  FCurrentSearchLine := id;
  UpdateLogDataPopupMenu;
end;

procedure TfrmMain.tvLogDataBeforeDrawNode(Sender: TObject; ACanvas: TCanvas;
  ARect: TRectF; ANode: TTMSFMXTreeViewVirtualNode; var AAllow,
  ADefaultDraw: Boolean);
begin
  if not tvLogData.IsNodeSelected(ANode.Node) then
    ACanvas.Fill.Color := LogEntryColors[TLogEntryType.FromString(ANode.Text[COL_LEVEL])];
end;

procedure TfrmMain.tvLogDataKeyDown(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  if ((Shift = []) or (Shift = [ssShift])) and TCharacter.IsLetterOrDigit(KeyChar) then
    StartSearch;
end;

procedure TfrmMain.txtCaptionClick(Sender: TObject);
begin
  txtOpenClick(Sender);
end;

procedure TfrmMain.txtClearFiltersClick(Sender: TObject);
begin
  TClearFilterViewsAction.Post;
end;

procedure TfrmMain.txtErrorsClick(Sender: TObject);
begin
  TToggleEntryTypeFilterAction.Post(TLogEntryType.ERROR);
end;

procedure TfrmMain.txtFilterExclusiveClick(Sender: TObject);
begin
  TAddTextExcludeFilterAction.Post(txtSearch.Text);
end;

procedure TfrmMain.txtFilterInclusiveClick(Sender: TObject);
begin
  TAddTextIncludeFilterAction.Post(txtSearch.Text);
end;

procedure TfrmMain.txtInfoClick(Sender: TObject);
begin
  TToggleEntryTypeFilterAction.Post(TLogEntryType.INFO);
end;

procedure TfrmMain.txtMenuClick(Sender: TObject);
begin
  if tvFiles.Width = 0 then
  begin
    TAnimator.AnimateFloat(tvFiles, 'Width', FFilesWidth);
    splLeft.Visible := True;
    seMenu.Enabled := False;
  end else
  begin
    FFilesWidth := tvFiles.Width;
    TAnimator.AnimateFloat(tvFiles, 'Width', 0);
    splLeft.Visible := False;
    seMenu.Enabled := True;
  end;
  //Self.Invalidate;
end;

procedure TfrmMain.txtOpenClick(Sender: TObject);
begin
  if dlgOpen.Execute then
  begin
    for var sFile in dlgOpen.Files do
    begin
      TOpenLogFileAction.Post(sFile);
    end;
  end;
end;

procedure TfrmMain.txtSearchChange(Sender: TObject);
begin
  txtSearchPrior.Enabled     := not txtSearch.Text.IsEmpty;
  txtSearchNext.Enabled      := not txtSearch.Text.IsEmpty;
  txtFilterInclusive.Enabled := not txtSearch.Text.IsEmpty;
  txtFilterExclusive.Enabled := not txtSearch.Text.IsEmpty;
end;

procedure TfrmMain.txtSearchKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  if Key = vkReturn then
    if FLastSearchWasNext then
      SearchNext
    else
      SearchPrior;
end;

procedure TfrmMain.txtSearchNextClick(Sender: TObject);
begin
  SearchNext;
end;

procedure TfrmMain.txtSearchPriorClick(Sender: TObject);
begin
  SearchPrior;
end;

procedure TfrmMain.txtTraceDebugClick(Sender: TObject);
begin
  TToggleEntryTypeFilterAction.Post(TLogEntryType.DEBUG);
end;

procedure TfrmMain.txtWarningsClick(Sender: TObject);
begin
  TToggleEntryTypeFilterAction.Post(TLogEntryType.WARN);
end;

procedure TfrmMain.UpdateAppFiles(Files: TDictionary<string, TList<TFileRecord>>);
var
  bFound : boolean;
begin
  var ary := Files.ToArray;
  for var a in ary do
  begin
    bFound := False;
    for var i := 0 to tviApplications.Count-1 do
    begin
      if tviApplications.Items[i].Text = a.Key then
      begin
        bFound := True;
        FillFromParent(tviApplications.Items[i], a.Value);
        break;
      end;
    end;
    if not bFound then
    begin
      var tvi := TTreeViewItem.Create(tvFiles);
      tvi.Parent := tviApplications;
      tvi.Text := a.Key;
      FillFromParent(tvi, a.Value);
    end;
  end;
end;

procedure TfrmMain.UpdateDownloads(Files: TList<TFileRecord>);
  function FindParent(Text : string) : TTreeViewItem;
  var
    i : integer;
  begin
    Result := nil;
    for i := 0 to tviDownloads.Count-1 do
    begin
      if tviDownloads.Items[i].Text = Text then
      begin
        Result := tviDownloads.Items[i];
        break;
      end;
    end;
    if not Assigned(Result) then
    begin
      Result := TTreeViewItem.Create(tvFiles);
      Result.Text := Text;
      Result.Parent := tviDownloads;
    end;
  end;
  procedure AddFile(Parent : TTreeViewItem; FileInfo : TFileRecord);
  begin
    var tvi := TFileTreeViewItem.Create(tvFiles);
    tvi.Item := FileInfo;
    tvi.Parent := Parent;
  end;
begin
  tviDownloads.BeginUpdate;
  try
    while tviDownloads.Count > 0 do
    begin
      var tvi := tviDownloads.Items[0];
      tvi.Parent := nil;
      tvi.Free;
    end;
    for var fi in Files do
    begin
      if fi.RelativeFilePath = fi.FileName then
        AddFile(tviDownloads, fi)
      else
        AddFile(FindParent(fi.RelativeFilePath.Replace(fi.FileName,'')), fi);
    end;
  finally
    tviDownloads.EndUpdate;
  end;
end;

procedure TfrmMain.UpdateFiles(Files: TList<TFileRecord>);
begin
  FillFromParent(tviOther, Files);
end;

{ TFileTreeViewItem }

constructor TFileTreeViewItem.Create(AOwner: TComponent);
begin
  inherited;
  FFileSize := TText.Create(Self);
end;

destructor TFileTreeViewItem.Destroy;
begin

  inherited;
end;

procedure TFileTreeViewItem.SetItem(const Value: TFileRecord);
  function FileSizeToText(Size : UInt64) : string;
  var
    kb, mb, gb : Double;
  begin
    kb := Size / 1024;
    mb := kb / 1000;
    gb := mb / 1000;
    if gb > 1 then
      Result := (Round(gb * 100) / 100).ToString+' GB'
    else if mb > 1 then
      Result := (Round(mb * 100) / 100).ToString+' MB'
    else if kb > 1 then
      Result := (Round(kb * 100) / 100).ToString+' KB'
    else
      Result := Size.ToString+' Bytes';
  end;
begin
  FItem := Value;
  FFileSize.Parent := Self;
  FFileSize.Align := TAlignLayout.Right;
  FFileSize.Text := FileSizeToText(Item.Size);
  FFileSize.Width := 75;
  FFileSize.HitTest := False;
  FFileSize.Locked := False;
  Text := Value.FileName;
end;

end.
