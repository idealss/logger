unit ideal.logger.viewer.actions;

interface

uses
  System.Classes,
  System.SysUtils,
  cocinasync.flux.action,
  ideal.logger.viewer.types;

type
  TViewLogFileAction = class(TBaseAction)
  private
    FFilePath : string;
  public
    class procedure Post(const AFilePath : string);
    property FilePath : string read FFilePath;
  end;

  TCompareLogFilesAction = class(TBaseAction)
  private
    FFile1 : string;
    FFile2 : string;
  public
    class procedure Post(const AFile1, AFile2 : string);
    property File1: string read FFile1;
    property File2: string read FFile2;
  end;

  TOpenLogFileAction = class(TBaseAction)
  private
    FFilePath : string;
  public
    class procedure Post(const AFilePath : string);
    property FilePath : string read FFilePath;
  end;

  TCloseLogFileAction = class(TBaseAction)
  private
    FFilePath : string;
  public
    class procedure Post(const AFilePath : string);
    property FilePath : string read FFilePath;
  end;

  TDeleteLogFileAction = class(TBaseAction)
  private
    FFilePath : string;
  public
    class procedure Post(const AFilePath : string);
    property FilePath : string read FFilePath;
  end;

  TSelectLogEntryAction = class(TBaseAction)
  private
    FFile : TFileRecord;
    FLine : ILineGroupInfo;
  public
    class procedure Post(const SelectedFile : TFileRecord; Line : ILineGroupInfo);
    property &File : TFileRecord read FFile;
    property Line : ILineGroupInfo read FLine;
  end;

  TShowDetailAction = class(TBaseAction)
  private
    FFilePath : String;
    FLine : ILineGroupInfo;
    FDetails : string;
    FSource : string;
  public
    class procedure Post(const FilePath : string; Line : ILineGroupInfo; const Details, Source : string);
    property FilePath : string read FFilePath;
    property Line : ILineGroupInfo read FLine;
    property Details : string read FDetails;
    property Source : string read FSource;
  end;

  TToggleEntryTypeFilterAction = class(TBaseAction)
  private
    FEntryType : TLogEntryType;
  public
    class procedure Post(EntryType : TLogEntryType);
    property EntryType : TLogEntryType read FEntryType;
  end;

  TAddTextIncludeFilterAction = class(TBaseAction)
  private
    FText : string;
  public
    class procedure Post(Text : string);
    property Text : string read FText;
  end;

  TAddTextExcludeFilterAction = class(TBaseAction)
  private
    FText : string;
  public
    class procedure Post(Text : string);
    property Text : string read FText;
  end;

  TIncludeThreadFilterAction = class(TBaseAction)
  private
    FThreadID : UInt64;
  public
    class procedure Post(ThreadID : UInt64);
    property ThreadID : UInt64 read FThreadID;
  end;

  TExcludeThreadFilterAction = class(TBaseAction)
  private
    FThreadID : UInt64;
  public
    class procedure Post(ThreadID : UInt64);
    property ThreadID : UInt64 read FThreadID;
  end;

  TSelectFilterViewAction = class(TBaseAction)
  private
    FView : IFilterView;
  public
    class procedure Post(View : IFilterView);
    property View : IFilterView read FView;
  end;

  TRemoveFilterViewAction = class(TBaseAction)
  private
    FView : IFilterView;
  public
    class procedure Post(View : IFilterView);
    property View : IFilterView read FView;
  end;

  TReverseFilterViewAction = class(TBaseAction)
  private
    FView : IFilterView;
  public
    class procedure Post(View : IFilterView);
    property View : IFilterView read FView;
  end;

  TReparentFilterViewAction = class(TBaseAction)
  private
    FView : IFilterView;
    FNewParent : IFilterView;
  public
    class procedure Post(View, NewParent : IFilterView);
    property View : IFilterView read FView;
    property NewParent : IFilterView read FNewParent;
  end;

  TSetLineFromFilterAction = class(TBaseAction)
  private
    FLine : UInt64;
  public
    class procedure Post(Line : UInt64);
    property Line : UInt64 read FLine;
  end;

  TSetLineToFilterAction = class(TBaseAction)
  private
    FLine : UInt64;
  public
    class procedure Post(Line : UInt64);
    property Line : UInt64 read FLine;
  end;

  TSetDeltaMinFilterAction = class(TBaseAction)
  private
    FMS : UInt64;
  public
    class procedure Post(MS : UInt64);
    property MS: UInt64 read FMS;
  end;

  TSetDurationMinFilterAction = class(TBaseAction)
  private
    FMS : UInt64;
  public
    class procedure Post(MS : UInt64);
    property MS: UInt64 read FMS;
  end;

  TSetDurationMaxFilterAction = class(TBaseAction)
  private
    FMS : UInt64;
  public
    class procedure Post(MS : UInt64);
    property MS: UInt64 read FMS;
  end;

  TSetStampAfterFilterAction = class(TBaseAction)
  private
    FStamp : TDateTime;
  public
    class procedure Post(Stamp : TDateTime);
    property Stamp : TDateTime read FStamp;
  end;

  TSetStampBeforeFilterAction = class(TBaseAction)
  private
    FStamp : TDateTime;
  public
    class procedure Post(Stamp : TDateTime);
    property Stamp : TDateTime read FStamp;
  end;

  TSetHideProfileNoDetailFilterAction = class(TBaseAction)
  public
    class procedure Post;
  end;

  TLoadViewsForFileAction = class(TBaseAction)
  private
    FDataFile : TFilerecord;
  public
    class procedure Post(DataFile : TFilerecord);
    property DataFile : TFilerecord read FDataFile;
  end;

  TClearFilterViewsAction = class(TBaseAction)
  public
    class procedure Post;
  end;


  TRefreshOpenFileAction = class(TBaseAction)
  public
    class procedure Post;
  end;

implementation

{ TSelectLogFileAction }

class procedure TOpenLogFileAction.Post(const AFilePath: string);
begin
  var act := inherited New<TOpenLogFileAction>;
  act.FFilePath := AFilePath;
  act.Dispatch;
end;

{ TCloseLogFileAction }

class procedure TCloseLogFileAction.Post(const AFilePath: string);
begin
  var act := inherited New<TCloseLogFileAction>;
  act.FFilePath := AFilePath;
  act.Dispatch;
end;

{ TDeleteLogFileAction }

class procedure TDeleteLogFileAction.Post(const AFilePath: string);
begin
  var act := inherited New<TDeleteLogFileAction>;
  act.FFilePath := AFilePath;
  act.Dispatch;
end;

{ TCompareLogFilesAction }

class procedure TCompareLogFilesAction.Post(const AFile1, AFile2: string);
begin
  var act := inherited New<TCompareLogFilesAction>;
  act.FFile1 := AFile1;
  act.FFile2 := AFile2;
  act.Dispatch;
end;

{ TViewLogFileAction }

class procedure TViewLogFileAction.Post(const AFilePath: string);
begin
  var act := inherited New<TViewLogFileAction>;
  act.FFilePath := AFilePath;
  act.Dispatch;
end;

{ TSelectLogEntryAction }

class procedure TSelectLogEntryAction.Post(const SelectedFile: TFileRecord;
  Line: ILineGroupInfo);
begin
  var act := inherited New<TSelectLogEntryAction>;
  act.FFile := SelectedFile;
  act.FLine := Line;
  act.Dispatch;
end;

{ TShowDetailAction }

class procedure TShowDetailAction.Post(const FilePath: string; Line: ILineGroupInfo; const Details, Source: string);
begin
  var act := inherited New<TShowDetailAction>;
  act.FFilePath := FilePath;
  act.FLine := Line;
  act.FDetails := Details;
  act.FSource := Source;
  act.Dispatch;
end;

{ TToggleEntryTypeFilterAction }

class procedure TToggleEntryTypeFilterAction.Post(EntryType: TLogEntryType);
begin
  var act := inherited New<TToggleEntryTypeFilterAction>;
  act.FEntryType := EntryType;
  act.Dispatch;
end;

{ TAddTextIncludeFilterAction }

class procedure TAddTextIncludeFilterAction.Post(Text: string);
begin
  var act := inherited New<TAddTextIncludeFilterAction>;
  act.FText := Text;
  act.Dispatch;
end;

{ TAddTextExcludeFilterAction }

class procedure TAddTextExcludeFilterAction.Post(Text: string);
begin
  var act := inherited New<TAddTextExcludeFilterAction>;
  act.FText := Text;
  act.Dispatch;
end;

{ TIncludeThreadFilterAction }

class procedure TIncludeThreadFilterAction.Post(ThreadID: UInt64);
begin
  var act := inherited New<TIncludeThreadFilterAction>;
  act.FThreadID := ThreadID;
  act.Dispatch;
end;

{ TExcludeThreadFilterAction }

class procedure TExcludeThreadFilterAction.Post(ThreadID: UInt64);
begin
  var act := inherited New<TExcludeThreadFilterAction>;
  act.FThreadID := ThreadID;
  act.Dispatch;
end;

{ TSelectFilterViewAction }

class procedure TSelectFilterViewAction.Post(View: IFilterView);
begin
  var act := inherited New<TSelectFilterViewAction>;
  act.FView := View;
  act.Dispatch;
end;

{ TRefreshOpenFileAction }

class procedure TRefreshOpenFileAction.Post;
begin
  (inherited New<TRefreshOpenFileAction>).Dispatch;
end;

{ TReparentFilterViewAction }

class procedure TReparentFilterViewAction.Post(View, NewParent: IFilterView);
begin
  var act := inherited New<TReparentFilterViewAction>;
  act.FView := View;
  act.FNewParent := NewParent;
  act.Dispatch;
end;

{ TLoadViewsForFileAction }

class procedure TLoadViewsForFileAction.Post(DataFile: TFilerecord);
begin
  var act := inherited New<TLoadViewsForFileAction>;
  act.FDataFile := DataFile;
  act.Dispatch;
end;

{ TSetLineFromFilterAction }

class procedure TSetLineFromFilterAction.Post(Line: UInt64);
begin
  var act := inherited New<TSetLineFromFilterAction>;
  act.FLine := Line;
  act.Dispatch;
end;

{ TSetLineToFilterAction }

class procedure TSetLineToFilterAction.Post(Line: UInt64);
begin
  var act := inherited New<TSetLineToFilterAction>;
  act.FLine := Line;
  act.Dispatch;
end;

{ TSetDurationMinFilterAction }

class procedure TSetDurationMinFilterAction.Post(MS: UInt64);
begin
  var act := inherited New<TSetDurationMinFilterAction>;
  act.FMS := MS;
  act.Dispatch;
end;

{ TSetDurationMaxFilterAction }

class procedure TSetDurationMaxFilterAction.Post(MS: UInt64);
begin
  var act := inherited New<TSetDurationMaxFilterAction>;
  act.FMS := MS;
  act.Dispatch;
end;

{ TSetStampAfterFilterAction }

class procedure TSetStampAfterFilterAction.Post(Stamp: TDateTime);
begin
  var act := inherited New<TSetStampAfterFilterAction>;
  act.FStamp := Stamp;
  act.Dispatch;
end;

{ TSetStampBeforeFilterAction }

class procedure TSetStampBeforeFilterAction.Post(Stamp: TDateTime);
begin
  var act := inherited New<TSetStampBeforeFilterAction>;
  act.FStamp := Stamp;
  act.Dispatch;
end;

{ TSetHideProfileNoDetailFilterAction }

class procedure TSetHideProfileNoDetailFilterAction.Post;
begin
  (inherited New<TSetHideProfileNoDetailFilterAction>).Dispatch;
end;

{ TSetDeltaMinFilterAction }

class procedure TSetDeltaMinFilterAction.Post(MS: UInt64);
begin
  var act := inherited New<TSetDeltaMinFilterAction>;
  act.FMS := MS;
  act.Dispatch;
end;

{ TClearFilterViewsAction }

class procedure TClearFilterViewsAction.Post;
begin
  (inherited New<TClearFilterViewsAction>).Dispatch;
end;

{ TRemoveFilterViewAction }

class procedure TRemoveFilterViewAction.Post(View: IFilterView);
begin
  var act := inherited New<TRemoveFilterViewAction>;
  act.FView := View;
  act.Dispatch;
end;

{ TReverseFilterViewAction }

class procedure TReverseFilterViewAction.Post(View: IFilterView);
begin
  var act := inherited New<TReverseFilterViewAction>;
  act.FView := View;
  act.Dispatch;
end;

end.
