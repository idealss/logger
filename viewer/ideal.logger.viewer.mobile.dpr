program ideal.logger.viewer.mobile;

uses
  System.StartUpCopy,
  FMX.Forms,
  ideal.logger.viewer.forms.sff.main in 'ideal.logger.viewer.forms.sff.main.pas' {Form1},
  ideal.logger.viewer.types in 'ideal.logger.viewer.types.pas',
  ideal.logger.viewer.tree in 'ideal.logger.viewer.tree.pas',
  ideal.logger.viewer.stores.views in 'ideal.logger.viewer.stores.views.pas',
  ideal.logger.viewer.stores.open in 'ideal.logger.viewer.stores.open.pas',
  ideal.logger.viewer.stores.files in 'ideal.logger.viewer.stores.files.pas',
  ideal.logger.viewer.actions in 'ideal.logger.viewer.actions.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
