unit ideal.logger.viewer.stores.open;

interface

uses
  System.Classes,
  System.SysUtils,
  System.Generics.Collections,
  System.UITypes,
  cocinasync.flux.store,
  cocinasync.Async,
  ideal.logger.viewer.types,
  ideal.logger.viewer.actions;

type
  TOpenStore = class(TBaseStore)
  strict private
    class var FData : TOpenStore;
  strict private
    type
      TLineGroupInfo = class(TInterfacedObject, ILineGroupInfo)
      private
        FLineFrom : UInt64;
        FLineTo : UInt64;
        FDuration : Double;
        FText : string;
        FNotes : string;
        FItems : TList<ILineGroupInfo>;
        FDelta : Int64;
        FTimestamp : TDateTime;
        FThread : Int64;
        FLevel : String;
        FOffset : String;

        function GetLineFrom : UInt64;
        function GetLineTo : UInt64;
        function GetDuration : Double;
        function GetText : string;
        function GetNotes : string;
        function GetDelta : Int64;
        function GetItems : TList<ILineGroupInfo>;
        function GetTimestamp : TDateTime;
        function GetThread : Int64;
        function GetLevel : String;
        function GetOffset : String;
      public
        property LineFrom : UInt64 read GetLineFrom;
        property LineTo : UInt64 read GetLineTo;
        property Duration : Double read GetDuration;
        property Notes : string read GetNotes;
        property Delta : Int64 read GetDelta;
        property Items : TList<ILineGroupInfo> read GetItems;
        property Text : string read GetText;
        property Timestamp : TDateTime read GetTimestamp;
        property Thread : Int64 read GetThread;
        property Level : String read GetLevel;
        property Offset : String read GetOffset;

        constructor Create(Line : TLineInfo); reintroduce;
        destructor Destroy; override;
      end;


  strict private
    FMultiThreads : TList<ILineGroupInfo>;
    FIsFiltered : boolean;
    FFile : TFileStream;
    FFilePath : string;
    FLines : TList<TLineInfo>;
    FThreads : TList<TPair<UInt64, TList<ILineGroupInfo>>>;
    FEntryTypeFilter : TLogEntryTypes;

    procedure OnViewLogFile(Sender : TObject; Action : TViewLogFileAction);
    procedure OnShowDetail(Sender : TObject; Action : TSelectLogEntryAction);
    procedure ProcessLineInfo;
    procedure ApplyFilters;
  public
    constructor Create; override;
    destructor Destroy; override;

    class property Data : TOpenStore read FData;
    class constructor Create;
    class destructor Destroy;

    function AutoViewUpdate: Boolean; override;

    property IsFiltered : boolean read FIsFiltered;
    property FilePath : String read FFilePath;
    property Threads : TList<TPair<UInt64, TList<ILineGroupInfo>>> read FThreads;
    property EntryTypeFilter : TLogEntryTypes read FEntryTypeFilter;
    function List : TList<ILineGroupInfo>;
    function DetailForLine(LineNumber : UInt64; var Source : String) : string;
  end;

implementation

uses
  System.IOUtils,
  System.Zip,
  chimera.json,
  ideal.logger.viewer.stores.views,
  cocinasync.jobs,
  cocinasync.flux,
  cocinasync.flux.fmx.actions.ui;

{ TOpenStore }

constructor TOpenStore.Create;
begin
  inherited;
  FFile := nil;
  FIsFiltered := False;
  FEntryTypeFilter := [];
  FLines := TList<TLineInfo>.Create;
  FThreads := TList<TPair<UInt64, TList<ILineGroupInfo>>>.Create;
  FMultiThreads := TList<ILineGroupInfo>.Create;

  Flux.Register<TViewLogFileAction>(Self, OnViewLogFile);
  Flux.Register<TSelectLogEntryAction>(Self, OnShowDetail);

  Flux.Register<TRefreshOpenFileAction>(Self,
    procedure(Action : TRefreshOpenFileAction)
    begin
      UpdateViews;
    end
  );

  Flux.Register<TToggleEntryTypeFilterAction>(Self,
    procedure(Action : TToggleEntryTypeFilterAction)
    begin
      if FEntryTypeFilter.IsInSet(Action.EntryType) then
        Exclude(FEntryTypeFilter, Action.EntryType)
      else
        Include(FEntryTypeFilter, Action.EntryType);
      UpdateViews;
    end
  );
end;

procedure TOpenStore.ApplyFilters;
  function LookupListForThread(List : TList<TPair<UInt64, TList<ILineGroupInfo>>>; ThreadID : Int64) : TList<ILineGroupINfo>;
  var
    idx : integer;
  begin
    idx := -1;
    for var i := 0 to List.Count-1 do
    begin
      if List[i].Key = ThreadID then
      begin
        idx := i;
        break;
      end;
    end;
    if idx < 0 then
    begin
      Result := TList<ILineGroupINfo>.Create;
      List.Add(TPair<UInt64, TList<ILineGroupInfo>>.Create(ThreadID, Result));
    end else
      Result := List[idx].Value;
  end;
var
  ThreadStacks : TDictionary<Int64, TStack<TLineGroupInfo>>;
  Stack : TStack<TLineGroupInfo>;
  idx : integer;
  lgi : TLineGroupInfo;
  a: TPair<Int64, TStack<TLineGroupInfo>>;
  lstItems : TList<TPair<UInt64, TList<ILineGroupInfo>>>;
  lst : TList<ILineGroupInfo>;
begin
  lstItems := TList<TPair<UInt64, TList<ILineGroupInfo>>>.Create;
  try
    idx := 0;
    ThreadStacks := TDictionary<Int64, TStack<TLineGroupInfo>>.Create;
    try
      repeat
        if not ThreadStacks.ContainsKey(FLines[idx].Thread) then
        begin
          Stack := TStack<TLineGroupInfo>.Create;
          ThreadStacks.Add(FLines[idx].Thread, Stack);
        end else
          Stack := ThreadStacks[FLines[idx].Thread];

        if FLines[idx].Level = 'EXIT' then
        begin
          if (Stack.Count > 0) then
          begin
            if Stack.Peek.Text = FLines[idx].Msg then
            begin
              lgi := Stack.Pop;
              lgi.FDuration := FLines[idx].Duration;
              if lgi.Items.Count = 0 then
                lgi.FLevel := 'PROFILE'
              else
                lgi.FLevel := '';
            end else
            begin
              var StackWalker := TStack<TLineGroupInfo>.Create;
              repeat
                StackWalker.Push(Stack.Pop);
              until (Stack.Count = 0) or (Stack.Peek.Text = FLines[idx].Msg);
              if Stack.Count > 0 then
              begin
                lgi := Stack.Pop;
                lgi.FDuration := FLines[idx].Duration;
                if lgi.Items.Count = 0 then
                  lgi.FLevel := 'PROFILE'
                else
                  lgi.FLevel := '';
              end;
              while StackWalker.Count > 0 do
                Stack.Push(StackWalker.Pop);
            end;
          end;
        end else
        begin
          lgi := TLineGroupInfo.Create(FLines[idx]);

          if Stack.Count = 0 then
          begin
            lst := LookupListForThread(lstItems, lgi.Thread);
            lst.Add(lgi);
          end else
            Stack.Peek.Items.Add(lgi);

          if FLines[idx].Level = 'ENTER' then
            Stack.Push(lgi);
        end;
        inc(idx);
      until idx >= FLines.Count;
    finally
      for a in ThreadStacks.ToArray do
      begin
        a.Value.Free;
      end;
      ThreadStacks.Free;
    end;
    TAsync.SynchronizeIfInThread(
      procedure
      begin
        var tmp := FThreads;
        FThreads := lstItems;
        lstItems := tmp;
      end
    );
  finally
    for var p in lstItems do
      p.Value.Free;
    lstItems.Free;
  end;
end;

function TOpenStore.AutoViewUpdate: Boolean;
begin
  Result := False;
end;

class constructor TOpenStore.Create;
begin
  FData := TOpenStore.Create;
end;

destructor TOpenStore.Destroy;
begin
  FFile.Free;
  FLines.Free;
  for var p in FThreads do
    p.Value.Free;
  FThreads.Free;
  FMultiThreads.Free;
  inherited;
end;

class destructor TOpenStore.Destroy;
begin
  FData.Free;
end;

function TOpenStore.DetailForLine(LineNumber: UInt64; var Source : String): string;
  function ExtractAndFormat(jso : IJSONObject) : string;
    function ExtractError(jso : IJSONObject) : string;
    begin
      Result := 'Exception Class: '+jso.Objects['@x'].StringsDefaulted['class']+#13#10+
                '        Message: '+jso.Objects['@x'].StringsDefaulted['message']+#13#10+
                '           Info: '+jso.Strings['@m']+#13#10#13#10+
                '      Callstack: '+jso.Objects['@x'].StringsDefaulted['callstack']+#13#10+
                jso.Objects['@detail'].AsJSON(TWhitespace.pretty);
    end;
    function ExtractDefault(jso : IJSONObject) : string;
    begin
      Result := jso.Strings['@m']+#13#10#13#10+
                jso.Objects['@detail'].AsJSON(TWhitespace.pretty);
    end;
  begin
    if jso.StringsDefaulted['@l'] = 'ERROR' then
      Result := ExtractError(jso)
    else
      Result := ExtractDefault(jso);
  end;
var
  i : integer;
  ln : TLineInfo;
  ary : TArray<Byte>;
begin

  if not ((LineNumber < FLines.Count-1) and (LineNumber >= 0) and (FLines[LineNumber].LineNumber = LineNumber)) then
  begin
    for i := 0 to FLines.Count-1 do
    begin
      if FLines[i].LineNumber = LineNumber then
      begin
        ln := FLines[i];
        break;
      end;
    end;
    if i = FLines.Count then
      exit;
  end else
    ln := FLines[LineNumber];

  Ffile.Position := ln.StartPOS;
  SetLength(ary,ln.Length);
  FFile.Read(ary, ln.Length);
  Source := TEncoding.UTF8.GetString(ary);

  Result := ExtractAndFormat(TJSON.From(Source));
end;

function TOpenStore.List : TList<ILineGroupInfo>;

  procedure FilterMergedList(View : IFilterView);
    function CheckFilterForItem(item : ILineGroupInfo) : boolean;
    begin
      Result := FEntryTypeFilter.IsEmpty or FEntryTypeFilter.IsInSet(Item.Level);

      if Result and (length(View.TextInclusive) > 0) then
      begin
        var b := False;
        for var s in View.TextInclusive do
        begin
          b := b or item.Text.ToUpper.Contains(s.ToUpper);
          if b then
            break;
        end;
        Result := b;
      end;

      if Result and (Length(View.TextExclusive) > 0) then
      begin
        var b := False;
        for var s in View.TextExclusive do
        begin
          b := b or item.Text.ToUpper.Contains(s.ToUpper);
          if b then
            break;
        end;
        Result := b;
      end;

      if Result and (View.StampAfter > DEFAULT_STAMP_AFTER) then
        Result := item.Timestamp > View.StampAfter;

      if Result and (View.StampBefore < DEFAULT_STAMP_BEFORE) then
        Result := item.Timestamp < View.StampBefore;

      if Result and (View.LineFrom > DEFAULT_LINE_FROM) then
        Result := item.LineFrom > View.LineFrom;

      if Result and (View.LineTo < DEFAULT_LINE_TO) then
        Result := item.LineTo < View.LineTo;

      if Result and (View.DeltaMin > DEFAULT_DELTA_MIN) then
        Result := item.Delta >= View.DeltaMin;

      if Result and (View.DurationMin > DEFAULT_DURATION_MIN) then
        Result := item.Duration >= View.DurationMin;

      if Result and (View.DurationMax < DEFAULT_DURATION_MAX) then
        Result := item.Duration <= View.DurationMax;

      if Result and (View.HideProfileNoDetail) then
        Result := (TLogEntryType.FromString(item.Level) <> TLogEntryType.PROFILE) or (item.Items.Count > 0);
    end;

    function InternalFilter(item : ILineGroupInfo) : boolean;
    var
      list : TList<ILineGroupInfo>;
      bItemResult : boolean;
    begin
      Result := False;
      if not Assigned(Item) then
        list := FMultiThreads
      else
        list := item.items;
      for var idx := list.Count-1 downto 0 do
      begin
        bItemResult := InternalFilter(list[idx]);
        if not bItemResult then
        begin
          FIsFiltered := True;
          List.Delete(idx);
        end;

        Result := Result or bItemResult;
      end;
      if (not Result) and Assigned(Item) then
        Result := CheckFilterForItem(item);
    end;
  begin
    InternalFilter(nil);
  end;

  procedure MergeLists(All, Thread : TList<ILineGroupInfo>);
  begin
    if All.Count = 0 then
    begin
      All.AddRange(Thread.ToArray);
      exit;
    end;

    if Thread.Count = 0 then
      exit;

    for var ir in Thread do
    begin
      var idx := 0;
      repeat
        if ir.LineFrom < All[idx].LineFrom then
          break;
        inc(idx);
      until (idx >= All.Count);

      if idx >= All.Count then
        All.Add(ir)
      else
        All.Insert(idx,ir);
    end;
  end;
var
  i : integer;
  bMatches : boolean;
  View : IFilterView;
begin
  Result := nil;
  FIsFiltered := False;

  View := TViewsStore.Data.Current.CompositedView;

  FMultiThreads.Clear;
  Result := FMultiThreads;
  for i := 0 to FThreads.Count-1 do
  begin
    if Length(View.IncludeThreadIDs) > 0 then
    begin
      FIsFiltered := True;
      bMatches := True;
      for var id in View.IncludeThreadIDs do
      begin
        if FThreads[i].Key = id then
        begin
          bMatches := False;
          break;
        end;
      end;
    end else
      bMatches := False;

    if Length(View.ExcludeThreadIDs) > 0 then
    begin
      FIsFiltered := True;
      bMatches := False;
      for var id in View.ExcludeThreadIDs do
      begin
        if FThreads[i].Key = id then
        begin
          bMatches := True;
          break;
        end;
      end;
    end;

    if (not bMatches) or (not FIsFiltered) then
    begin
      MergeLists(FMultiThreads, FThreads[i].Value);
    end;
  end;

  FilterMergedList(View);
end;

procedure TOpenStore.OnShowDetail(Sender: TObject; Action: TSelectLogEntryAction);
var
  sDetail, sSource : string;
begin
  sDetail := DetailForLine(Action.Line.LineFrom, sSource);
  TShowDetailAction.Post(Action.&File.Filename, Action.Line, sDetail, TJSON.Format(sSource));
end;

procedure TOpenStore.OnViewLogFile(Sender: TObject; Action: TViewLogFileAction);
begin
  FEntryTypeFilter := [];
  TJobManager.Execute(
    procedure
      function ExtractJSONFileToTempFile(const Filepath : string) : string;
      begin
        Result := TPath.Combine(TPath.GetTempPath, ChangeFileExt(ExtractFileName(FilePath), '.json'));
        TZipfile.ExtractZipFile(Filepath, ExtractFilePath(Result));
      end;
    var
      sFilePath : string;
    begin
      if Assigned(FFile) then
        FFile.Free;
      FFilepath := Action.FilePath;
      if ExtractFileExt(FFilePath).ToLower = '.jsonz' then
        sFilePath := ExtractJSONFileToTempFile(FFilePath)
      else
        sFilePath := FFilePath;
      FFile := TFileStream.Create(sFilePath, fmOpenRead or fmShareDenyNone);
      ProcessLineInfo();
      ApplyFilters();
      UpdateViews;
    end
  );
end;

procedure TOpenStore.ProcessLineInfo;
const
  BUFF_SIZE = 4096;
var
  buff : TArray<Byte>;
  iBuffLength : integer;
  iFSBufferStart : integer;
  iLastLinePos : integer;
  iBuffEOLIdx : integer;
  i: integer;
begin
  iFSBufferStart := 0;
  iBuffEOLIdx := 0;
  iLastLinePos := 0;
  FFile.Position := 0;
  SetLength(buff, BUFF_SIZE);
  repeat
    iFSBufferStart := FFile.Position;
    iBuffLength := FFile.Read(buff, BUFF_SIZE);
    if iBuffLength > 0 then
    begin
      for i := 0 to iBuffLength-1 do
      begin
        if (buff[i] = 13) or (buff[i] = 10) then
        begin
          iBuffEOLIdx := i;
          if iBuffEOLIdx+iFSBufferStart-iLastLinePos > 1 then
            FLines.Add(TLineInfo.From(FFile, iLastLinePos, (iBuffEOLIdx+iFSBufferStart)-iLastLinePos));
          iLastLinePos := iFSBufferStart+i+1;
        end;
      end;
    end;
  until FFile.Position >= FFile.Size;
end;

{ TOpenStore.TLineGroupInfo }

constructor TOpenStore.TLineGroupInfo.Create(Line : TLineInfo);
begin
  inherited Create;
  FItems := TList<ILineGroupInfo>.Create;
  FLineFrom := Line.LineNumber;
  FLineTo := Line.LineNumber;
  FDuration := Line.Duration;
  FDelta := Line.Delta;
  FText := Line.Msg;
  FNotes := '';
  FThread := Line.Thread;
  FLevel := Line.Level;
  FOffset := Line.Offset;
  FTimestamp := Line.Timestamp;
end;

destructor TOpenStore.TLineGroupInfo.Destroy;
begin
  FItems.Free;
  inherited;
end;

function TOpenStore.TLineGroupInfo.GetDelta: Int64;
begin
  Result := FDelta;
end;

function TOpenStore.TLineGroupInfo.GetDuration: Double;
begin
  Result := FDuration;
end;

function TOpenStore.TLineGroupInfo.GetItems: TList<ILineGroupInfo>;
begin
  Result := FItems;
end;

function TOpenStore.TLineGroupInfo.GetLevel: String;
begin
  Result := FLevel;
end;

function TOpenStore.TLineGroupInfo.GetLineFrom: UInt64;
begin
  Result := FLineFrom;
end;

function TOpenStore.TLineGroupInfo.GetLineTo: UInt64;
begin
  Result := FLineTo;
end;

function TOpenStore.TLineGroupInfo.GetNotes: string;
begin
  Result := FNotes;
end;

function TOpenStore.TLineGroupInfo.GetOffset: String;
begin
  Result := FOffset;
end;

function TOpenStore.TLineGroupInfo.GetText: string;
begin
  Result := FText;
end;

function TOpenStore.TLineGroupInfo.GetThread: Int64;
begin
  Result := FThread;
end;

function TOpenStore.TLineGroupInfo.GetTimestamp: TDateTime;
begin
  Result := FTimestamp;
end;


end.
